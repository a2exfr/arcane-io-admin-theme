
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small>Statistics Overview</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

				<div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-comments fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">26</div>
                                        <div>New Comments!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-tasks fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">12</div>
                                        <div>New Tasks!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-shopping-cart fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">124</div>
                                        <div>New Orders!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-support fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">13</div>
                                        <div>Support Tickets!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
				
				<div class="row">
				      <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-users"></i> New Users To Aprove</h3>
                            </div>
                            <div class="panel-body">
                                <div class="list-group">
                                    <a href="#" class="list-group-item">
                                        <span class="badge">just now</span>
                                        <i class="fa fa-fw fa-calendar"></i> Calendar updated
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">4 minutes ago</span>
                                        <i class="fa fa-fw fa-comment"></i> Commented on a post
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">23 minutes ago</span>
                                        <i class="fa fa-fw fa-truck"></i> Order 392 shipped
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">46 minutes ago</span>
                                        <i class="fa fa-fw fa-money"></i> Invoice 653 has been paid
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">1 hour ago</span>
                                        <i class="fa fa-fw fa-user"></i> A new user has been added
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">2 hours ago</span>
                                        <i class="fa fa-fw fa-check"></i> Completed task: "pick up dry cleaning"
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">yesterday</span>
                                        <i class="fa fa-fw fa-globe"></i> Saved the world
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">two days ago</span>
                                        <i class="fa fa-fw fa-check"></i> Completed task: "fix error on sales page"
                                    </a>
                                </div>
                                <div class="text-right">
                                    <a href="#">View All  <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>	
					<div class="col-lg-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-file-text"></i> New Content Added</h3>
                            </div>
                            <div class="panel-body">
                                <div class="list-group">
                                    <a href="#" class="list-group-item">
                                        <span class="badge">just now</span>
                                        <i class="fa fa-fw fa-calendar"></i> Calendar updated
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">4 minutes ago</span>
                                        <i class="fa fa-fw fa-comment"></i> Commented on a post
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">23 minutes ago</span>
                                        <i class="fa fa-fw fa-truck"></i> Order 392 shipped
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">46 minutes ago</span>
                                        <i class="fa fa-fw fa-money"></i> Invoice 653 has been paid
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">1 hour ago</span>
                                        <i class="fa fa-fw fa-user"></i> A new user has been added
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">2 hours ago</span>
                                        <i class="fa fa-fw fa-check"></i> Completed task: "pick up dry cleaning"
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">yesterday</span>
                                        <i class="fa fa-fw fa-globe"></i> Saved the world
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">two days ago</span>
                                        <i class="fa fa-fw fa-check"></i> Completed task: "fix error on sales page"
                                    </a>
                                </div>
                                <div class="text-right">
                                    <a href="#">View All <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>		
					<div class="col-lg-4">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-users"></i> New Content</h3>
                            </div>
                            <div class="panel-body">
                                <div class="list-group">
                                    <a href="#" class="list-group-item">
                                        <span class="badge">just now</span>
                                        <i class="fa fa-fw fa-calendar"></i> Calendar updated
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">4 minutes ago</span>
                                        <i class="fa fa-fw fa-comment"></i> Commented on a post
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">23 minutes ago</span>
                                        <i class="fa fa-fw fa-truck"></i> Order 392 shipped
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">46 minutes ago</span>
                                        <i class="fa fa-fw fa-money"></i> Invoice 653 has been paid
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">1 hour ago</span>
                                        <i class="fa fa-fw fa-user"></i> A new user has been added
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">2 hours ago</span>
                                        <i class="fa fa-fw fa-check"></i> Completed task: "pick up dry cleaning"
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">yesterday</span>
                                        <i class="fa fa-fw fa-globe"></i> Saved the world
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">two days ago</span>
                                        <i class="fa fa-fw fa-check"></i> Completed task: "fix error on sales page"
                                    </a>
                                </div>
                                <div class="text-right">
                                    <a href="#">View All  <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
                <div class="row">
                    <div class="col-lg-12">
						<p style="text-align:justify;">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tincidunt malesuada feugiat. Suspendisse faucibus odio rutrum augue dignissim feugiat. Curabitur non odio a nisi egestas cursus id vel ante. Nullam eget fermentum erat, et fermentum orci. Suspendisse porta aliquet convallis. Vestibulum gravida lectus eu massa vehicula, eu porttitor nunc cursus. Vestibulum gravida scelerisque aliquet. Maecenas fermentum purus orci. Cras ipsum lectus, scelerisque ac congue et, luctus dictum eros. Etiam at arcu quis ex condimentum egestas. Pellentesque euismod ex vel orci vulputate finibus. Praesent varius tempus erat, ut laoreet est tincidunt non. Sed feugiat eget lectus sed lacinia. Aliquam erat volutpat. Vestibulum non tempor est.<br />

						Nullam eget ornare libero. Nam erat leo, rhoncus mollis justo et, pretium eleifend arcu. Proin venenatis aliquam ornare. Morbi enim elit, venenatis a ullamcorper at, sodales sed felis. Fusce commodo, augue eget accumsan tempus, ex ligula porta quam, sit amet mattis sapien nisl vitae sapien. Pellentesque lacinia nec tortor sed dignissim. Pellentesque facilisis vel ex eget eleifend. Proin volutpat ligula tortor, a ullamcorper mi volutpat ut.<br />

						Donec vel est dui. Praesent posuere libero at est lobortis, sed interdum erat elementum. Ut dapibus a lacus nec feugiat. Nunc enim est, molestie vitae nisl vitae, sodales egestas arcu. Donec a ligula ipsum. Etiam auctor tincidunt elementum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur auctor est urna, ornare blandit elit vestibulum ut.<br />

						Nullam porta in turpis a maximus. Aenean consectetur augue nec elit tincidunt, consectetur vehicula tortor ultricies. Vivamus ultrices est non augue dignissim, lobortis tempus nibh mattis. Aenean nulla sapien, gravida ut nisi molestie, malesuada pretium purus. Nam in aliquam quam. Curabitur at auctor lectus. Etiam blandit cursus augue, ut interdum massa. Nulla magna nunc, commodo in arcu ut, accumsan pellentesque odio. Curabitur ut vehicula risus. Nunc facilisis justo lectus, sed condimentum elit euismod id. Praesent lacinia purus ac massa cursus, a luctus libero dictum. Quisque non facilisis est. Maecenas non ligula consequat, iaculis ipsum ut, sollicitudin lectus. Nunc tristique, turpis vel posuere placerat, ipsum arcu eleifend diam, id ornare diam orci id nisi.<br />

						Vestibulum blandit id urna id elementum. Nunc aliquet arcu eget augue dapibus tempus. Mauris varius ultrices porta. Fusce vestibulum finibus tellus, in maximus erat. Phasellus tempor cursus condimentum. Integer tortor elit, tristique eget blandit at, porta nec leo. Ut pellentesque, tortor sed posuere gravida, ex elit bibendum est, in rutrum felis diam nec nibh. Ut nec velit augue. Mauris placerat augue ut lacus laoreet, eget pretium nisi iaculis. Integer mi massa, rutrum eu felis id, tristique sodales ipsum. Curabitur consectetur, justo ac cursus finibus, arcu metus iaculis ex, quis blandit elit quam a tortor. Pellentesque eget nibh ac orci pretium tincidunt nec sit amet mi. Praesent vehicula sem nec interdum faucibus. Sed commodo, leo id tincidunt elementum, elit neque fermentum lorem, condimentum fringilla enim elit accumsan velit. Donec vel hendrerit est.<br />

						In nisi enim, accumsan nec ullamcorper placerat, dapibus a nisl. Proin molestie varius dignissim. Fusce dapibus, lectus a fringilla interdum, diam massa tincidunt nibh, ultricies euismod justo velit vel magna. Donec id lectus eu nisl lobortis sagittis. Etiam maximus eu lorem vel semper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eleifend eros et augue fermentum, vel tempor lorem euismod. Cras neque ligula, blandit quis lacus eget, bibendum egestas urna.<br />

						Nunc imperdiet libero quam, ut pretium leo placerat ut. Mauris faucibus accumsan sagittis. Praesent vitae porta massa. Nunc vitae maximus mi. Nulla iaculis lorem ut venenatis pulvinar. Pellentesque eu pellentesque est. Morbi congue viverra fringilla. Maecenas sagittis fringilla mattis. Integer ut viverra leo. Cras tincidunt at arcu et pellentesque. Nunc iaculis mauris ut tortor lacinia, ut gravida nunc feugiat. Mauris vitae justo odio. Integer purus quam, efficitur cursus fermentum id, vestibulum id odio. Donec nec bibendum urna.<br />

						Morbi id diam ut tortor tristique sagittis. In rutrum, enim vel aliquam efficitur, nibh eros dignissim lectus, et venenatis mauris felis eget est. Vivamus nec orci lacinia, posuere nisi sit amet, porttitor lectus. Nunc vel nisl nec metus iaculis iaculis. Maecenas consectetur felis et venenatis fermentum. Phasellus malesuada leo sed mauris cursus, id accumsan leo venenatis. Nunc et bibendum tortor. Aliquam efficitur, eros in porttitor egestas, lacus ipsum venenatis mauris, vitae volutpat nulla magna at ante. Etiam viverra turpis ultricies, venenatis lectus non, tincidunt arcu. Morbi id nibh porta, suscipit metus sed, lobortis tortor. Morbi sed ex nisl. Morbi iaculis erat et lacus pretium, rhoncus molestie leo hendrerit. Cras pulvinar porta congue. Morbi vehicula vehicula sem vitae consequat. Morbi ut diam varius, facilisis arcu at, mollis urna. Vivamus scelerisque sapien a velit laoreet blandit.<br />

						Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut pulvinar ipsum vitae purus auctor, at volutpat massa auctor. Fusce a nunc magna. Vestibulum cursus, diam id ornare bibendum, sem velit varius magna, vel ornare magna velit a turpis. Sed rutrum non dolor eu vestibulum. Sed feugiat, purus sagittis ultrices molestie, diam nisi pulvinar purus, et fringilla diam lorem sit amet nulla. Aliquam feugiat arcu eget libero consectetur lobortis. Suspendisse tempor, neque sit amet pulvinar egestas, est lacus consectetur risus, iaculis iaculis leo nunc non lacus. Proin vulputate ultrices dui sit amet viverra. Curabitur elementum ipsum lorem, sit amet aliquam quam vulputate eget. Curabitur bibendum justo eu accumsan eleifend. Maecenas sed dapibus lectus. Mauris et scelerisque augue, nec egestas nisl.<br />

						Integer efficitur libero a sapien ullamcorper, vitae vestibulum mi feugiat. Cras pulvinar egestas nulla, ac auctor lorem interdum vitae. Nunc iaculis augue id mi ornare placerat. Fusce pellentesque neque eu ex posuere venenatis. Nam vel aliquet neque. Morbi pharetra, quam a aliquam consequat, nisl dolor tempus est, in ornare diam mi non nulla. Mauris nec ante sit amet ex blandit tincidunt. Vivamus vulputate quam quis nisl maximus consequat. <br />
						</p>
					
                    </div>
                   
                </div>
               
            

          

            </div>
            <!-- /.container-fluid -->
