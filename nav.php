        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" ng-controller="nav" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Arcane IO Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" target="_self"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-footer">
                            <a href="#">Read All New Messages</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" target="_self"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" target="_self"><img class="img-circle avatar" src="/img/avatar.jpg" />Dzidzio Smith <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li ng-class="{ active: isActive('/')}" >
                        <a href="/"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" target="_self" data-toggle="collapse" data-target="#content"><i class="fa fa-fw fa-file-text"></i> Content <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="content" class="collapse">
                            <li>
                                <a href="#"><i class="fa fa-fw fa-plus"></i> Add Content</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-fw fa-file-text"></i> Categories</a>
                            </li>
                        </ul>

				   </li>
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#users"><i class="fa fa-fw fa-users"></i> Users <i class="fa fa-fw fa-caret-down"></i></a>
						<ul id="users" class="collapse">
                            <li>
                                <a href="#"><i class="fa fa-fw fa-plus"></i> Add User</a>
                            </li>
                            <li ng-class="{ active: isActive('/users')}">
                                <a href="#!users"><i class="fa fa-fw fa-list"></i> All Users</a>
                            </li>
                        </ul>
                    </li>
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#loc" target="_self"><i class="fa fa-fw fa-globe"></i> Localizations <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="loc" class="collapse">
                            <li>
                                <a href="#"><i class="fa fa-fw fa-list"></i> Countries</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-fw fa-list"></i> Lexicons</a>
                            </li>
							<li>
                                <a href="#"><i class="fa fa-fw fa-circle-o"></i> Dictionaries</a>
                            </li>
                        </ul>

				   </li>
				   <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#settings"><i class="fa fa-fw fa-cog"></i> Settings <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="settings" class="collapse">
                            <li>
                                <a href="#"><i class="fa fa-fw fa-link"></i> SEO</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-fw fa-circle-o"></i> Item2</a>
                            </li>
							<li>
                                <a href="#"><i class="fa fa-fw fa-circle-o"></i> Item3</a>
                            </li>
                        </ul>

				   </li>
					
                    <li ng-class="{ active: isActive('/tables')}">
                        <a href="#!tables"><i class="fa fa-fw fa-table"></i> Tables</a>
                    </li>
                    <li>
                        <a href="#!forms"><i class="fa fa-fw fa-edit"></i> Forms</a>
                    </li>
					<li ng-class="{ active: isActive('/bootstrap-elements')}" >
                        <a href="#!bootstrap-elements"><i class="fa fa-fw fa-desktop"></i> Bootstrap Elements</a>
                    </li>
                 
                    <li ng-class="{ active: isActive('/bootstrap-grid')}" >
                        <a href="#!bootstrap-grid"><i class="fa fa-fw fa-wrench"></i> Bootstrap Grid</a>
                    </li>

                    <li ng-class="{ active: isActive('/blank-page')}" >
                        <a href="#!blank-page"><i class="fa fa-fw fa-file"></i> Blank Page</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>