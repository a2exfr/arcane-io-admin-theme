            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Users
                            <small>Subheading</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-users"></i> Users
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
				
				<div class="row">
                    <div class="col-lg-12">
					        <div class="table-responsive">
							
							  <table id="mytable" class="table table-bordred table-striped">
								   
								<thead>
									<th>ID</th>
									<th>Name</th>
									<th>Mail</th>
									<th>Category</th>
									<th>Register</th>
									<th>Update</th>
									<th>Last</th>
									
								</thead>
								
								<tbody>
									<tr>
										<td>1</td>
										<td class="list_user_name">
											<img class="img-circle pull-left" src="http://placehold.it/32x32"/>
											<a href="#" target="_self" >Dzidzio</a>
										</td>
										<td>example@gmail.com</td>
										<td>Administrator</td>
										<td>20-01-2017</td>
										<td>21-01-2017</td>
										<td>22-01-2017</td>
									</tr>									
									
									<tr>
										<td>2</td>
										<td class="list_user_name">
											<img class="img-circle pull-left" src="http://placehold.it/32x32"/>
											<a href="#" target="_self" >Dzidzio</a>
										</td>
										<td>example@gmail.com</td>
										<td>Administrator</td>
										<td>20-01-2017</td>
										<td>21-01-2017</td>
										<td>22-01-2017</td>
									</tr>									
									
									<tr>
										<td>3</td>
										<td class="list_user_name">
											<img class="img-circle pull-left" src="http://placehold.it/32x32"/>
											<a href="#" target="_self" >Dzidzio</a>
										</td>
										<td>example@gmail.com</td>
										<td>Administrator</td>
										<td>20-01-2017</td>
										<td>21-01-2017</td>
										<td>22-01-2017</td>
									</tr>									
									
									<tr>
										<td>4</td>
										<td class="list_user_name">
											<img class="img-circle pull-left" src="http://placehold.it/32x32"/>
											<a href="#" target="_self" >Dzidzio</a>
										</td>
										<td>example@gmail.com</td>
										<td>Administrator</td>
										<td>20-01-2017</td>
										<td>21-01-2017</td>
										<td>22-01-2017</td>
									</tr>									
									
									<tr>
										<td>5</td>
										<td class="list_user_name">
											<img class="img-circle pull-left" src="http://placehold.it/32x32"/>
											<a href="#" target="_self" >Dzidzio</a>
										</td>
										<td>example@gmail.com</td>
										<td>Administrator</td>
										<td>20-01-2017</td>
										<td>21-01-2017</td>
										<td>22-01-2017</td>
									</tr>
									
								</tbody>
								
							</table>

					<div class="clearfix"></div>
						<ul class="pagination pull-right">
						  <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
						  <li class="active"><a href="#">1</a></li>
						  <li><a href="#">2</a></li>
						  <li><a href="#">3</a></li>
						  <li><a href="#">4</a></li>
						  <li><a href="#">5</a></li>
						  <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
						</ul>
                
					</div>
								
					</div>
				</div> <!-- /.row -->
				
				<div class="row">
                    <div class="col-lg-12">
					        <div class="table-responsive">
							
							  <table id="mytable" class="table table-hover">
								   
								<thead>
									<th>ID</th>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Category</th>
									<th>Email</th>
									<th>Contact</th>
									<th>Actions</th>
								</thead>
								
								<tbody>
									<tr>
									<td>1</td>
									<td>Dzidzio</td>
									<td>Smith</td>
									<td>Administrator</td>
									<td>example@gmail.com</td>
									<td>+3805011111111</td>
									<td>
										<div class="dropdown">
											<a href="#" target="_self" data-toggle="dropdown" aria-expanded="false">
												<p><i class="fa fa-ellipsis-v mail-icon"></i></p>
											</a>
											<ul class="dropdown-menu float-right">
												<li>
													<a href="#" title="">
														<i class="fa fa-envelope"></i>
														Message
													</a>
												</li>	
												<li>
													<a href="#" title="">
														<i class="fa fa-pencil"></i>
														Edit
													</a>
												</li>
												<li>
													<a href="#" title="">
														<i class="fa fa-ban"></i>
														Disable
													</a>
												</li>
												<li>
													<a href="#" title="">
														<i class="fa fa-trash-o"></i>
														Delete
													</a>
												</li>
											</ul>
										</div>
									</td>
									</tr>
									
									<tr>
									<td>1</td>
									<td>Dzidzio</td>
									<td>Smith</td>
									<td>Administrator</td>
									<td>example@gmail.com</td>
									<td>+3805011111111</td>
									<td>
										<div class="dropdown">
											<a href="#" target="_self" data-toggle="dropdown" aria-expanded="false">
												<p><i class="fa fa-ellipsis-v mail-icon"></i></p>
											</a>
											<ul class="dropdown-menu float-right">
												<li>
													<a href="#" title="">
														<i class="fa fa-envelope"></i>
														Message
													</a>
												</li>	
												<li>
													<a href="#" title="">
														<i class="fa fa-pencil"></i>
														Edit
													</a>
												</li>
												<li>
													<a href="#" title="">
														<i class="fa fa-ban"></i>
														Disable
													</a>
												</li>
												<li>
													<a href="#" title="">
														<i class="fa fa-trash-o"></i>
														Delete
													</a>
												</li>
											</ul>
										</div>
									</td>
									</tr>
									
									<tr>
									<td>1</td>
									<td>Dzidzio</td>
									<td>Smith</td>
									<td>Administrator</td>
									<td>example@gmail.com</td>
									<td>+3805011111111</td>
									<td>
										<div class="dropdown">
											<a href="#" target="_self" data-toggle="dropdown" aria-expanded="false">
												<p><i class="fa fa-ellipsis-v mail-icon"></i></p>
											</a>
											<ul class="dropdown-menu float-right">
												<li>
													<a href="#" title="">
														<i class="fa fa-envelope"></i>
														Message
													</a>
												</li>	
												<li>
													<a href="#" title="">
														<i class="fa fa-pencil"></i>
														Edit
													</a>
												</li>
												<li>
													<a href="#" title="">
														<i class="fa fa-ban"></i>
														Disable
													</a>
												</li>
												<li>
													<a href="#" title="">
														<i class="fa fa-trash-o"></i>
														Delete
													</a>
												</li>
											</ul>
										</div>
									</td>
									</tr>
									
									<tr>
									<td>1</td>
									<td>Dzidzio</td>
									<td>Smith</td>
									<td>Administrator</td>
									<td>example@gmail.com</td>
									<td>+3805011111111</td>
									<td>
										<div class="dropdown">
											<a href="#" target="_self" data-toggle="dropdown" aria-expanded="false">
												<p><i class="fa fa-ellipsis-v mail-icon"></i></p>
											</a>
											<ul class="dropdown-menu float-right">
												<li>
													<a href="#" title="">
														<i class="fa fa-envelope"></i>
														Message
													</a>
												</li>	
												<li>
													<a href="#" title="">
														<i class="fa fa-pencil"></i>
														Edit
													</a>
												</li>
												<li>
													<a href="#" title="">
														<i class="fa fa-ban"></i>
														Disable
													</a>
												</li>
												<li>
													<a href="#" title="">
														<i class="fa fa-trash-o"></i>
														Delete
													</a>
												</li>
											</ul>
										</div>
									</td>
									</tr>
									
									<tr>
									<td>1</td>
									<td>Dzidzio</td>
									<td>Smith</td>
									<td>Administrator</td>
									<td>example@gmail.com</td>
									<td>+3805011111111</td>
									<td>
										<div class="dropdown">
											<a href="#" target="_self" data-toggle="dropdown" aria-expanded="false">
												<p><i class="fa fa-ellipsis-v mail-icon"></i></p>
											</a>
											<ul class="dropdown-menu float-right">
												<li>
													<a href="#" title="">
														<i class="fa fa-envelope"></i>
														Message
													</a>
												</li>	
												<li>
													<a href="#" title="">
														<i class="fa fa-pencil"></i>
														Edit
													</a>
												</li>
												<li>
													<a href="#" title="">
														<i class="fa fa-ban"></i>
														Disable
													</a>
												</li>
												<li>
													<a href="#" title="">
														<i class="fa fa-trash-o"></i>
														Delete
													</a>
												</li>
											</ul>
										</div>
									</td>
									</tr>
									
									<tr>
									<td>1</td>
									<td>Dzidzio</td>
									<td>Smith</td>
									<td>Administrator</td>
									<td>example@gmail.com</td>
									<td>+3805011111111</td>
									<td>
										<div class="dropdown">
											<a href="#" target="_self" data-toggle="dropdown" aria-expanded="false">
												<p><i class="fa fa-ellipsis-v mail-icon"></i></p>
											</a>
											<ul class="dropdown-menu float-right">
												<li>
													<a href="#" title="">
														<i class="fa fa-envelope"></i>
														Message
													</a>
												</li>	
												<li>
													<a href="#" title="">
														<i class="fa fa-pencil"></i>
														Edit
													</a>
												</li>
												<li>
													<a href="#" title="">
														<i class="fa fa-ban"></i>
														Disable
													</a>
												</li>
												<li>
													<a href="#" title="">
														<i class="fa fa-trash-o"></i>
														Delete
													</a>
												</li>
											</ul>
										</div>
									</td>
									</tr>
							
								</tbody>
								
							</table>

					<div class="clearfix"></div>
						<ul class="pagination pull-right">
						  <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
						  <li class="active"><a href="#">1</a></li>
						  <li><a href="#">2</a></li>
						  <li><a href="#">3</a></li>
						  <li><a href="#">4</a></li>
						  <li><a href="#">5</a></li>
						  <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
						</ul>
                
					</div>
								
					</div>
				</div> <!-- /.row -->
				
				
				
				
				
            </div>
            <!-- /.container-fluid -->
