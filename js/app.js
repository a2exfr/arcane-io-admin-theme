

var app = angular.module("myApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
     .when("/", {
        templateUrl : "dashboard.php"
    }) 
     .when("/forms", {
        templateUrl : "forms.php"
    })
	.when("/users", {
        templateUrl : "users.php"
    })
    .when("/tables", {
        templateUrl : "tables.php"
    })
    .when("/bootstrap-elements", {
        templateUrl : "bootstrap-elements.php"
    })  
	.when("/bootstrap-grid", {
        templateUrl : "bootstrap-grid.php"
    })
	.when("/blank-page", {
        templateUrl : "blank-page.php"
    })
	.otherwise({
        template : "<h1>None</h1><p>Nothing has been selected</p>"
    })	;
	// $locationProvider.html5Mode(true);
});

/* app.controller('myCtrl', function($scope) {
    $scope.firstName = "John";
    $scope.lastName = "Doe";

	
	}); 

function HeaderController($scope, $location){ 
    $scope.isActive = function (viewLocation) { 
        return viewLocation === $location.path();
    };
}
*/
app.controller('nav', function($scope,$location) {
    $scope.isActive = function (viewLocation) { 
        return viewLocation === $location.path();
    };
}); 
